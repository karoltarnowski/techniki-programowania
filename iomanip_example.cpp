/*iomanip_example.cpp
  na podstawie: http://www.cplusplus.com*/

#include<iostream>
#include<iomanip>
/*Biblioteka iomanip dostarcza następujące modifikatory:
  - setbase,
  - setfill,
  - setprecision,
  - setw.
*/

using namespace std;

int main(){
    cout << setbase(16);  //setbase zmienia podstawę systemu w jakim są wypisywane liczby
    cout << 110 << endl;  //liczba 110 wypisana jako szesnastkowe 6e
    cout << oct;          //podobny efekt można uzyskać wstawiając do strumienia: dec, hex, oct
    cout << 110 << endl;  //liczba 110 wypisana jako ósemkowe 156

    cout << setfill('x'); //setfill ustawia znak jakim są wypełniane pola
    cout << setw (10);    //setw ustawia szerokość pola dla następnej danej
    cout << 77 << endl;   //77 wypisane jako xxxxxxx115
    // szerokość pola 10
    // zapis w systemie ósemkowym to 115
    // pole jest wypełnione znakami 'x'

    cout.fill('_');       //podobny efekt jak setfill można uzyskać metodą fill() obiektu cout
    cout << setw (10);
    cout << 77 << endl;   //77 wypisane jako _______115

    double f = 3.14159;
    cout << setprecision(5) << f << endl; //setprecision ustawia liczbę cyfr znaczących
    cout << setprecision(9) << f << endl;
    cout.precision(3);                    //podobny efekt można uzyskać metodą precision() obiektu cout
    cout << f << endl;
    cout.precision(12);
    cout << f << endl;
}














