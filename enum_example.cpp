/*enum_example.cpp*/
/*Program pokazujący możliwość definicji typu wyliczeniowego.
Program jednocześnie wykorzystuje generator liczb pseudolosowych.*/

#include <iostream>
#include <cstdlib>  //wykorzystywane funkcje: srand(), rand()
#include <ctime>    //wykorzystywane funkcje: time()

using namespace std;

//deklaracja typu wyliczeniowego możliwych kolorów kart
enum card_suit{
    SPADE, HEART, DIAMOND, CLUB
};

//deklaracja typu wyliczeniowego możliwych wartości kart
enum card_rank{
    TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE
};

/*stałe tablice globalne łańcuchów znakowych wykorzystywane
przy wypisywaniu wartości kart*/
const char* card_suit_names[] = \
    {"pik  ", "kier ", "karo ", "trefl" };
const char* card_rank_names[] = \
    {" 2", " 3", " 4", " 5", " 6", " 7", " 8", " 9", "10", " J", " Q", " K", " A" };

int main(){
    //inicjalizacja generatora liczb pseudolosowych
    srand(time(NULL));

    //losowa wartość całkowita z przedziału 0-3 (typ int)
    //jest rzutowana na typ card_suit
    card_suit c_suit = card_suit(rand()%4);
    //przypisanie c_suit = rand()%4 nie byłoby prawidłowe

    //wybór losowej wartości karty
    card_rank c_rank = card_rank(rand()%13);

    //wyświetlenie wartości karty z wykorzystaniem nazw kolorów i wartości
    cout << card_rank_names[c_rank] << " " << card_suit_names[c_suit] << endl;

    //wyświetlenie wartości kart
    cout << c_rank << " " << c_suit << endl;

    return 0;
}





