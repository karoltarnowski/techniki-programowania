#ifndef TABLETENNISPLAYER_H
#define TABLETENNISPLAYER_H
#include <string>
using std::string;

class TableTennisPlayer
{
    public:
        TableTennisPlayer(const string & fn = "brak", \
                          const string & ln = "brak", \
                          bool ht = false);
        void name() const;
        bool hasTable() const { return _has_table; };
        void resetTable(bool v) { _has_table = v; };

    private:
        string _first_name;
        string _last_name;
        bool   _has_table;
};

#endif // TABLETENNISPLAYER_H
