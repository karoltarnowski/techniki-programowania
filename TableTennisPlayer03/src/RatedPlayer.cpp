#include "RatedPlayer.h"

RatedPlayer::RatedPlayer(unsigned int r, \
  const string & fn, const string & ln, bool ht)
  : TableTennisPlayer(fn, ln, ht)
{
    _rating = r;
}

RatedPlayer::RatedPlayer(unsigned int r, const TableTennisPlayer & tp)
    : TableTennisPlayer(tp)
{
    _rating = r;
}
