#include <iostream>
#include "include\TableTennisPlayer.h"
#include "include\RatedPlayer.h"

void Show(const TableTennisPlayer & rt);

int main()
{
    RatedPlayer       rplayer(1140, "Maciej", "Kaczkowski", true);
    TableTennisPlayer & rt = rplayer;
    TableTennisPlayer * pt = &rplayer;
    rt.name();
    std::cout << std::endl;
    pt->name();
    std::cout << std::endl;

    TableTennisPlayer  player("Teresa", "Bogatko", false);
    //RatedPlayer & rr = player;  NIEDOZWOLONE
    //RatedPlayer * pr = &player; NIEDOZWOLONE

    Show(player);
    Show(rplayer);

    RatedPlayer olaf1(1840, "Olaf", "Bochenek", true);
    TableTennisPlayer olaf2(olaf1);
    //TableTennisPlayer(const RatedPlayer& );        nie ma takiego konstruktora
    //TableTennisPlayer(const TableTennisPlayer& );  ale jest taki niejawny konstruktor kopiujący

    TableTennisPlayer winner;
    winner = olaf1;  //przypisuje obiekt klasy pochodnej do obiektu klasy bazowej
    //TableTennisPlayer & operator=(const TableTennisPlayer &) const;

    return 0;
}

void Show(const TableTennisPlayer & rt){
    std::cout << "Nazwisko i imie: ";
    rt.name();
    std::cout << "\nStol: ";
    if( rt.hasTable() )
        std::cout << "tak\n";
    else
        std::cout << "nie\n";
}






















