/*hellowowrld.cpp*/

#include<iostream>
/*Zwróć uwagę na inny plik nagłówkowy
niż używany w C stdio.h*/

using namespace std;
/*Instrukcja ułatwia posługiwanie się
krótszymi wersjami niektórych procedur.
W dalszej części kursu rozwiniemy
to wyjaśnienie.*/

int main(){
    cout << "Hello world!\n";
    /*W tym miejscu posługujemy się obiektem cout - strumieniem wyjściowym.
    Wykorzystując operator wstawiania (<<), wstawiamy łańuch znakowy
    do strumienia, czyli wypisujemy go na ekran.*/

    /*jeżeli w funkcji main() nie ma instrukcji return,
    wtedy domyślnie zwracana jest wartość 0.*/
}
