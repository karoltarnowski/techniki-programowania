#include <iostream>
#include "AccountABC.h"

using std::cout;
using std::ios_base;
using std::endl;

using std::string;

AccountABC::AccountABC(const string& s, long an, double bal)
    : _full_name(s)
    , _account_number(an)
    , _balance(bal)
{}

void AccountABC::deposit(double amount){
    if( amount < 0 )
        cout << "Nie mozna wplacic ujemnej kwoty; "
             << "wplata anulowana." << endl;
    else
        _balance += amount;
}

void AccountABC::withdraw(double amount){
    _balance =- amount;
}

AccountABC::Formatting AccountABC::setFormat() const{
    Formatting f;
    f.flag = cout.setf(ios_base::fixed, ios_base::floatfield);
    f.pr = cout.precision(2);
    return f;
}

void AccountABC::restore(Formatting& f) const{
    cout.setf(f.flag, ios_base::floatfield);
    cout.precision(f.pr);
}

void Account::withdraw(double amount){
    Formatting f = setFormat();

    if( amount < 0 )
        cout << "Nie mozna wyplacic ujemnej kwoty; "
             << "wyplata anulowana." << endl;
    else if( amount < balance() )
        AccountABC::withdraw(amount);
    else
        cout << "Zadana suma " << amount
             << " PLN przekracza dostepne srodki." << endl
             << "Wyplata anulowana." << endl;

     restore(f);
}

void Account::viewAccount() const{
    Formatting f = setFormat();
    cout << "Klient: " << fullName() << endl;
    cout << "Numer rachunku: " << accountNumber() << endl;
    cout << "Stan konta: " << balance() << " PLN" << endl;
    restore(f);
}

AccountPlus::AccountPlus(const string& s, long an, double bal,
                         double ml, double r)
    : AccountABC(s, an, bal)
    , _max_loan(ml)
    , _rate(r)
    , _owes(0)
{}

AccountPlus::AccountPlus(const Account & a, double ml, double r)
    : AccountABC(a)
    , _max_loan(ml)
    , _rate(r)
    , _owes(0)
{}

void AccountPlus::withdraw(double amount){
    Formatting f = setFormat();

    double bal = balance();
    if( amount < bal )
        AccountABC::withdraw(amount);
    else if( amount < bal + _max_loan - _owes ){
        double advance = amount - bal;
        _owes += advance * (1.0 + _rate);
        cout << "Zadluzenie faktyczne: " << advance << " PLN" << endl;
        cout << "Odsetki: " << advance * _rate << " PLN" << endl;
        deposit(advance);
        AccountABC::withdraw(amount);
    }
    else
        cout << "Przekroczony limit debetu. Operacja anulowana" << endl;

    restore(f);
}

void AccountPlus::viewAccount() const{
    Formatting f = setFormat();
    cout << "Klient: " << fullName() << endl;
    cout << "Numer rachunku: " << accountNumber() << endl;
    cout << "Stan konta: " << balance() << " PLN" << endl;
    cout << "Limit debetu: " << _max_loan << endl;
    cout << "Kwota zadluzenia: " << _owes << endl;
    cout.precision(3);
    cout << "Stopa oprocentowania: " << 100*_rate << "%" << endl;
    restore(f);
}


