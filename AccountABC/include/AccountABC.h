#ifndef ACCOUNTABC_H
#define ACCOUNTABC_H
#include <iostream>
#include <string>

class AccountABC
{
    public:
        AccountABC(const std::string & s = "brak", long an = -1, double bal = 0.0);
        void deposit(double amount);
        virtual void withdraw(double amount) = 0;
        double balance() const {return _balance; };
        virtual void viewAccount() const = 0;
        virtual ~AccountABC() {};

    protected:
        struct Formatting{
            std::ios_base::fmtflags flag;
            std::streamsize pr;
        };
        const std::string & fullName() const {return _full_name; }
        long accountNumber() const { return _account_number; }
        Formatting setFormat() const;
        void restore(Formatting & f) const;

    private:
        std::string _full_name;
        long _account_number;
        double _balance;
};

class Account : public AccountABC
{
    public:
        Account(const std::string & s = "brak", long an = -1, double bal = 0.0)
            : AccountABC(s, an, bal){}
        virtual void withdraw(double amount);
        virtual void viewAccount() const;
        virtual ~Account() {};
};

class AccountPlus : public AccountABC
{
    public:
        AccountPlus(const std::string & s = "brak", long an = -1, double bal = 0.0,
                    double ml = 2000, double r = 0.11125);
        AccountPlus(const Account & a, double ml = 2000, double r = 0.11125);
        virtual void withdraw(double amount);
        virtual void viewAccount() const;
        void resetMax(double m){_max_loan = m; };
        void resetRate(double r){_rate = r; };
        void resetOwes(){_owes = 0;};

    private:
        double _max_loan;
        double _rate;
        double _owes;
};


#endif // ACCOUNTABC_H

