/*Program rozwiązuje układ równań Ax = b,
dla podanych macierzy A (rozmiaru 4x4)
oraz wektora b.*/
#include <iostream>
#include <gsl/gsl_linalg.h>

using namespace std;

int main (void)
{
    /*16 liczb tworzących macierz A.*/
    double a_data[] = { 0.18, 0.60, 0.57, 0.96,
                        0.41, 0.24, 0.99, 0.58,
                        0.14, 0.30, 0.97, 0.66,
                        0.51, 0.13, 0.19, 0.85 };

    /*Cztery liczby wektora b.*/
    double b_data[] = { 1.0, 2.0, 3.0, 4.0 };

    /*Struktury danych obsługujące macierz A i wektor b.*/
    gsl_matrix_view m
    = gsl_matrix_view_array (a_data, 4, 4);

    gsl_vector_view b
    = gsl_vector_view_array (b_data, 4);

    /*Alokacja pamięci na wektor rozwiązań x.*/
    gsl_vector *x = gsl_vector_alloc (4);

    /*Struktura obsługująca permutacje (wykorzystywana
    przez eliminację Gaussa z częściowym wyborem elementu głównego).*/
    gsl_permutation * p = gsl_permutation_alloc (4);
    /*Zmienna przechowujaca znak permutacji.*/
    int s;

    /*Funkcja:
    int gsl_linalg_complex_LU_decomp(gsl_matrix_complex * A, gsl_permutation * p, int * signum)
    Znajduje rozkład LU macierzy kwadratowej A:
    PA = LU
    Po wywołaniu macierz A zawiera macierz U w górnym trójkącie (z diagonalą).
    Dolny trójkąt (bez diagonali) zawiera macierz L.
    Elementy diagonalne macierzy L to jedynki (nie są przechowywane w pamięci).

    Informacje o permutacjach są zapisane w strukturze p.
    signum - znak permutacji.

    Funkcja używa algorytmu eliminacji Gaussa z częściowym wyborem elementu głównego
    (Golub & Van Loan, Matrix Computations, Algorithm 3.4.1).*/

    gsl_linalg_LU_decomp (&m.matrix, p, &s);

    /*Funkcja:
    int gsl_linalg_LU_solve(const gsl_matrix * LU, const gsl_permutation * p, const gsl_vector * b, gsl_vector * x)
    Rozwiązuje układ równań Ax = b.
    Na podstawie rozkładu LU, permutacji p i wektora b oblicza wektor x.*/

    gsl_linalg_LU_solve (&m.matrix, p, &b.vector, x);

    /*Wypisanie wektora rozwiązań na standardowe wyjście (stdout)*/
    cout << "x = " << endl;
    gsl_vector_fprintf (stdout, x, "%g");

    /*Zwolnienie pamięci roboczej*/
    gsl_permutation_free (p);
    gsl_vector_free (x);
    return 0;
}
