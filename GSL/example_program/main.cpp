#include <iostream>
#include <iomanip>
//dołączenie pliku nagłówkowego
#include <gsl/gsl_sf_bessel.h>

using namespace std;

int main (void)
{
  double x = 5.0;
  //wywołanie funkcji z biblioteki GSL
  double y = gsl_sf_bessel_J0 (x);
  cout << "J0(" << x << ") = ";
  cout << setprecision(18) << scientific << y << endl;
  return 0;
}
