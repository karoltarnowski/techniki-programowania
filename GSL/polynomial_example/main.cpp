/*Program wyznacza pierwiastki wielomianu
p(x) = -1 + x^5 korzystając z biblioteki GSL.*/
#include <iostream>
#include <iomanip>
#include <gsl/gsl_poly.h>

using namespace std;

int main (void)
{
    int i;
    /*Tablica a zawiera współczynniki wielomianu p(x).
    Wielomian stopnia 5 ma 6 współczynników,
    podawane są od wyrazu wolnego do współczynnika
    przy najwyższej potędze.*/
    double a[6] = { -1, 0, 0, 0, 0, 1 };

    /*Współrzędne 5 miejsc zerowych będą zapisane
    jako 5 liczb zespolonych, każda jako dwie liczby
    typu double, razem 10-elementów.*/
    double z[10];

    /*Alokacja pamięci roboczej wymaganej przez funkcję
    gsl_poly_complex_solve().*/
    gsl_poly_complex_workspace * w
    = gsl_poly_complex_workspace_alloc (6);

    /*Wywołanie funkcji
    int gsl_poly_complex_solve(const double * a, size_t n, gsl_poly_complex_workspace * w, gsl_complex_packed_ptr z)*/
    gsl_poly_complex_solve (a, 6, w, z);

    /*Zwolnienie pamięci roboczej*/
    gsl_poly_complex_workspace_free (w);

    cout << setprecision(18);
    for (i = 0; i < 5; i++){
        cout << "z" << noshowpos << i << " = ";
        cout << showpos << z[2*i] << z[2*i+1] << "i" << endl;
    }

    return 0;
}
