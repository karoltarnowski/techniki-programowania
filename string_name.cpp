/*string_name.cpp*/
/*Program pokazujący możliwość wykorzystania typu string*/

#include<iostream>
#include<string>
/*Aby korzystać z typu string, należy dołączyć bibliotekę string.
W odróżnieniu od typów wbudowanych typ string, nie jest dostępny.*/

using namespace std;

int main(){
    string name;        //deklaracja typu

    cout << "Podaj swoje imie: ";
    cin >> name;
    cout << "Czesc " << name << "!" << endl;
}
