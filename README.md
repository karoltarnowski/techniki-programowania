# Techniki programowania INP001002Wl #

Kody przykładowych programów w językach C oraz C++, wykorzystywanych
w ramach kursu Techniki programowania dla studentów
kierunku *Inżynieria kwantowa*.

Kody na podstawie książki:
A. Allain, *Przewodnik dla początkujących. C++*, Helion, 2014.
