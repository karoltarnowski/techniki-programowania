/*multidimensional_array_example.cpp*/
/*Program pokazujący możliwości działania
na tablicach wielowymiarowych.*/

#include <iostream>

using namespace std;

/*Deklaracje funkcji pobierających jako argument tablice wielowymiarowe.
Należy podać rozmiary tablicy - wszystkie poza pierwszym.
Pierwszy rozmiar tablicy można podać.*/
void print_matrix(double [3][4]);
void add_matrix(double [][4], double [][4]);

int main(){
    //Deklaracje tablic rozmiaru 3x4, połączone z inicjalizacją.
    double matrix_A[3][4] = { {-2, 1, 0, 0}, {1, -2, 1, 0}, {0, 1, -2, 1} };
    double matrix_B[3][4] = { {0, 0, 1, -2}, {0, 1, -2, 1}, {1, -2, 1, 0} };

    //Wyświetlenie zawartości tablicy
    cout << "macierz A" << endl;
    for(int i=0; i<3; i++){
        for(int j=0; j<4; j++)
            /*Odwołanie do elementu tablicy następuje przez podanie
            indeksów we wszystkich kierunkach.*/
            cout << matrix_A[i][j] << " " ;
        cout << endl;
    }

    //Wyświetlenie zawartości tablicy z wykorzystaniem funkcji.
    cout << "macierz B" << endl;
    print_matrix(matrix_B);

    //Wyowłanie funkcji obliczającej sumę dwóch macierzy.
    add_matrix(matrix_A,matrix_B);

    cout << "macierz A" << endl;
    print_matrix(matrix_A);
}

void print_matrix(double matrix[3][4]){
    for(int i=0; i<3; i++){
        for(int j=0; j<4; j++)
            cout << matrix[i][j] << " " ;
        cout << endl;
    }
}

/*Funkcja dodająca macierz b do macierzy a.
Elementy macierzy a są aktualizowane
(tablice przekazane przez referencję)*/
void add_matrix(double a[][4], double b[][4]){
    for(int i=0; i<3; i++)
        for(int j=0; j<4; j++)
            a[i][j] += b[i][j];
}









