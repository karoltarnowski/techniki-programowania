/*pascals_traingle.cpp*/
/*Program pokazujący możliwość alokacji pamięci
w formie tablicy dwuwymiarowej o różnych długościach
wierszy. Obliczane są wartości elementów trójkąta Pascala.

int** traingle wskazuje na tablicę wskaźników.
Każdy ze wskaźników pokazuje tablicę liczb całkowitych.*/

#include <iostream>
#include <iomanip>

using namespace std;

int main(){
    int n;
    //deklaracja wskaźnika triangle
    int **triangle;

    do{
        cout << "Podaj liczbe wierszy: ";
        cin >> n;
    }while(n<=0);

    /*Alokacja pamięci z wykorzystaniem operatora new.

    wskaznik = new typ[liczba elementów],
    gdzie wskaznik jest zmienną typ*

    Poniżej alokowana jest tablica n-elementowa typu int*,
    a triangle jest typu int**.
    */
    triangle = new  int* [n];

    //Indeks i przebiega po wierszach trójkąta Pascala.
    for(int i=0; i<n; i++){
        /*W każdym elemencie tablicy triangle zapamiętywany
        jest adres alokowanej i+1 elementowej tablicy liczb całkowitych.*/
        triangle[i] = new int[i+1];

        //pierwszy element w wierszu ma wartość 1
        triangle[i][0] = 1;

        //j-ty element w i-tym wierszu jest obliczany jako suma
        //elementów z wiersza i-1, o indeksach j i j-1.
        for(int j=1; j<i; j++)
            triangle[i][j] = triangle[i-1][j-1] + triangle[i-1][j];

        //ostatni element w wierszu ma wartość 1
        triangle[i][i] = 1;
    }

    //Wyświetlanie wartości elementów.
    for(int i=0; i<n; i++){
        for(int j=0; j<=i; j++)
            cout << setw(4) << triangle[i][j] << " ";
        cout << endl;
    }

    //Zwalnianie pamięci.
    for(int i=0; i<n; i++){
        //W pierwszej kolejności zwalniana jest pamięć z elementami w wierszach.
        delete[] triangle[i];
    }
    //W dalszej kolejności zwalniana jest tablica triangle.
    delete[] triangle;

}
