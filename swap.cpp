/*swap.cpp*/
/*Program pokazujący różne sposoby przekazywania
argumentów do funkcji:
- przez wartość (kopię),
- przez wskaźnik,
- przez referencję.*/

#include <iostream>

using namespace std;

void swap_value(int  left, int  right);
void swap_pointer(int* left, int* right);
void swap_reference(int& left, int& right);

int main(){
    int x = 1, y = 2;
    swap_value(x,y);
    cout << x << " " << y << endl;
    swap_pointer(&x,&y);
    cout << x << " " << y << endl;
    swap_reference(x,y);
    cout << x << " " << y << endl;
}

/*Funkcja pobiera kopie dwóch liczb całkowitych.
Oryginały się nie zamienią, gdyż funkcja
operuje na kopiach.*/
void swap_value(int left, int right){
    int temp = left;
    left = right;
    right = temp;
}

/*Funkcja pobiera wskaźniki do dwóch liczb całkowitych.
Wykorzystanie operatora wskaźnikowego pozwala
zamienić miejscami oryginały.*/
void swap_pointer(int* left, int* right){
    int temp = *left;
    *left = *right;
    *right = temp;
}

/*Funkcja pobiera referencje do dwóch liczb całkowitych.
Nie potrzebujemy operatora wskaźnikowego,
ale działamy na oryginałach argumentów.*/
void swap_reference(int& left, int& right){
    int temp = left;
    left = right;
    right = temp;
}





