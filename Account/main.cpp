#include <iostream>
#include "Account.h"

int main()
{
    using std::cout;
    using std::endl;

    Account piggy("Bonifacy Kot", 381299, 12000.00);
    AccountPlus hoggy("Horacy Biedronka", 382288, 9000.00);
    piggy.viewAccount();
    cout << endl;
    hoggy.viewAccount();
    cout << endl;

    cout << "Wplata 3000 PLN na rachunek pana Biedronki." << endl;
    hoggy.deposit(3000.00);
    cout << "Nowy stan konta: " << hoggy.balance() << " PLN" << endl;
    cout << "Wyplata 12600 PLN z rachunku pana Kota." << endl;
    piggy.withdraw(12600.00);
    cout << "Stan konta Kota: " << piggy.balance() << " PLN" << endl;
    cout << "Wyplata 12600 PLN z rachunku pana Biedronki." << endl;
    hoggy.withdraw(12600.00);
    hoggy.viewAccount();


    return 0;
}
