#include <iostream>
#include "Account.h"

using std::string;
using std::cout;
using std::endl;

typedef std::ios_base::fmtflags format;
typedef std::streamsize precis;
format setFormat();
void restore(format f, precis p);

Account::Account(const string& s, long an, double bal)
    : _full_name(s)
    , _account_number(an)
    , _balance(bal)
{}

void Account::deposit(double amount){
    if( amount < 0 )
        cout << "Nie mozna wplacic ujemnej kwoty; "
             << "wplata anulowana." << endl;
    else
        _balance += amount;
}

void Account::withdraw(double amount){
    format initial_state = setFormat();
    precis prec = cout.precision(2);
    if( amount < 0 )
        cout << "Nie mozna wyplacic ujemnej kwoty; "
             << "wyplata anulowana." << endl;
    else if( amount < _balance )
        _balance =- amount;
    else
        cout << "Zadana suma " << amount
             << " PLN przekracza dostepne srodki." << endl
             << "Wyplata anulowana." << endl;
    restore(initial_state, prec);
}

double Account::balance() const{
    return _balance;
}

void Account::viewAccount() const{
    format initial_state = setFormat();
    precis prec = cout.precision(2);
    cout << "Klient: " << _full_name << endl;
    cout << "Numer rachunku: " << _account_number << endl;
    cout << "Stan konta: " << _balance << " PLN" << endl;
    restore(initial_state, prec);
}

AccountPlus::AccountPlus(const string& s, long an, double bal,
                         double ml, double r)
    : Account(s, an, bal)
    , _max_loan(ml)
    , _rate(r)
    , _owes(0)
{}

AccountPlus::AccountPlus(const Account & a, double ml, double r)
    : Account(a)
    , _max_loan(ml)
    , _rate(r)
    , _owes(0)
{}

void AccountPlus::withdraw(double amount){
    format initial_state = setFormat();
    precis prec = cout.precision(2);

    double bal = balance();
    if( amount < bal )
        Account::withdraw(amount);
    else if( amount < bal + _max_loan - _owes ){
        double advance = amount - bal;
        _owes += advance * (1.0 + _rate);
        cout << "Zadluzenie faktyczne: " << advance << " PLN" << endl;
        cout << "Odsetki: " << advance * _rate << " PLN" << endl;
        deposit(advance);
        Account::withdraw(amount);
    }
    else
        cout << "Przekroczony limit debetu. Operacja anulowana" << endl;
    restore(initial_state, prec);
}

void AccountPlus::viewAccount() const{
    format initial_state = setFormat();
    precis prec = cout.precision(2);
    Account::viewAccount();
    cout << "Limit debetu: " << _max_loan << endl;
    cout << "Kwota zadluzenia: " << _owes << endl;
    cout.precision(3);
    cout << "Stopa oprocentowania: " << 100*_rate << "%" << endl;
    restore(initial_state, prec);
}

format setFormat(){
    return cout.setf(std::ios_base::fixed,
                     std::ios_base::floatfield);
}

void restore(format f, precis p){
    cout.setf(f, std::ios_base::floatfield);
    cout.precision(p);
}

