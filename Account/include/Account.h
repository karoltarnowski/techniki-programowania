#ifndef ACCOUNT_H
#define ACCOUNT_H
#include<string>

class Account
{
    public:
        Account(const std::string & s = "brak", long an = -1, double bal = 0.0);
        void deposit(double amount);
        virtual void withdraw(double amount);
        double balance() const;
        virtual void viewAccount() const;
        virtual ~Account() {};

    private:
        std::string _full_name;
        long _account_number;
        double _balance;
};

class AccountPlus : public Account
{
    public:
        AccountPlus(const std::string & s = "brak", long an = -1, double bal = 0.0,
                    double ml = 2000, double r = 0.11125);
        AccountPlus(const Account & a, double ml = 2000, double r = 0.11125);
        virtual void withdraw(double amount);
        virtual void viewAccount() const;
        void resetMax(double m){_max_loan = m; };
        void resetRate(double r){_rate = r; };
        void resetOwes(){_owes = 0;};

    private:
        double _max_loan;
        double _rate;
        double _owes;
};


#endif // ACCOUNT_H











