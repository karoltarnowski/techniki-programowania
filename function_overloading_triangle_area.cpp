/*function_overloading_triangle_area.cpp*/
/*Program pokazujący możliwość przeciążania funkcji
na przykładzie funkcji obliczających pole trójkąta.*/

#include<iostream>
#include<cmath> //sqrt()

using namespace std;

/*Deklaracja funkcji triangle_area().
  Zwróć uwagę na "podwójną" deklarację funkcji.
  Deklaracje różnią się różną listą argumentów:
  - pierwsza oblicza pole wykorzystując podstawę a oraz wysokość h,
  - druga oblicza pole wykorzystując długości boków: a, b, c.*/
double triangle_area(double a, double h);
double triangle_area(double a, double b, double c);

int main(){
    //deklaracja wykorzystywanych zmiennych
    double a, b, c, h;
    a = 4;
    h = 3;
    b = 3;
    c = 5;
    //wyświetlenie wyników
    cout << "Pole trojkata o podstawie " << a;
    cout << " oraz wysokosci " << h << " = ";
    cout << triangle_area(a,h) << "." << endl;

    cout << "Pole trojkata o bokach " << a << ", ";
    cout << b << ", " << c << " = ";
    cout << triangle_area(a,b,c) << "." << endl;
}

//Funkcja oblicza pole wykorzystując podstawę a oraz wysokość h.
double triangle_area(double a, double h){
    return a*h/2.;
}

/*Funkcja oblicza pole wykorzystując długości boków a, b, c.
  Implementuje wzór Herona.*/
double triangle_area(double a, double b, double c){
    double p = (a+b+c)/2.;
    return sqrt(p*(p-a)*(p-b)*(p-c));
}






















