#include <iostream>
#include <vector>

using namespace std;

int main()
{
    //deklaracja 4-elementowej tablicy liczb całkowitych
    int table[4];

    //deklaracja 4-elementowego wektora liczb całkowitych
    vector<int> vec(4);

    //zerowanie wszystkich elementów tablicy i wektora
    for(int i = 0; i<4; i++){
        table[i] = 0;
        vec[i]   = 0;
    }

    //wyświetlenie rozmiaru wektora
    cout << "rozmiar wektora: " << vec.size() << endl;

    //dodanie elementu do wektora (dopisanie na końcu)
    vec.push_back(17);

    //wyświetlenie elementów wektora z wykorzystaniem iteratora
    for( vector<int>::iterator itr = vec.begin();
         itr != vec.end();
         ++itr ){
        cout << *itr << endl;
    }
    cout << endl;

    //wyświetlenie elementów wektora z wykorzystaniem iteratora
    //bez wywoływania metody end() przy każdym przebiegu pętli
    for( vector<int>::iterator itr = vec.begin(), koniec = vec.end();
         itr != koniec;
         ++itr ){
        cout << *itr << endl;
    }
    cout << endl;

    return 0;
}













