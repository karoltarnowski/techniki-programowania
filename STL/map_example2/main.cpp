#include <iostream>
#include <map>
#include <string>

using namespace std;

//Funkcja wyswietlMape pobiera mapę przez referencję.
//Dodatkowo zadeklarowano referencję jako const,
//bo funkcja tylko wyświetla mapę; nie modyfikuje jej.
void wyswietlMape(const map<string, string>&);

int main()
{
    map<string, string> nazwa_email;

    nazwa_email["Alex Allain"] = "alex.allain@cprogramming.com";

    cout << nazwa_email["Alex Allain"] << endl;

    nazwa_email["Karol Tarnowski"] = "karol.tarnowski@pwr.edu.pl";

    map<string, string>::iterator itr = nazwa_email.find("Alex Allain");
    if( itr != nazwa_email.end() ){
        cout << "To adres Alexa: " << itr->second;
    }
    cout << endl;

    wyswietlMape(nazwa_email);

    return 0;
}

//Ponieważ argument mapa zadeklarowany jest jako const,
//to do obsługi mapy należy wykorzystać iterator,
//który nie pozwoli na zmianę kluczy/wartości mapy.
//Jest to const_iterator.
void wyswietlMape(const map<string, string>& mapa){
    for( map<string, string>::const_iterator itr = mapa.begin(), koniec = mapa.end();
         itr != koniec; ++itr ){
        cout << itr->first << " --> " << itr->second << endl;
    }
}















