#include <iostream>
#include <map>
#include <string>

using namespace std;

void wyswietlMape(map<string, string>);

int main()
{
    //deklaracja zmiennej typu mapa
    //klucz   typu string
    //wartość typu string
    map<string, string> nazwa_email;

    //dodanie do mapy nowego elementu
    //adresu e-mail: "alex.allain@cprogramming.com"
    //z kluczem:     "Alex Allain"
    nazwa_email["Alex Allain"] = "alex.allain@cprogramming.com";

    //wyświetlenie wartości powiązanej z kluczem "Alex Allain"
    cout << nazwa_email["Alex Allain"] << endl;

    //dodanie do mapy kolejnego elementu
    nazwa_email["Karol Tarnowski"] = "karol.tarnowski@pwr.edu.pl";

    //wykorzystanie metody find(), bo znalezienia zadanego elementu mapy
    map<string, string>::iterator itr = nazwa_email.find("Alex Allain");
    if( itr != nazwa_email.end() ){
        cout << "To adres Alexa: " << itr->second;
    }
    cout << endl;

    wyswietlMape(nazwa_email);

    return 0;
}

//funkcja wyswietlMape() wykorzystuje iterator
//aby przejść przez wyszystkie elementy mapy
//i je wyświetlić
void wyswietlMape(map<string, string> mapa){
    for( map<string, string>::iterator itr = mapa.begin(), koniec = mapa.end();
         itr != koniec; ++itr ){
        cout << itr->first << " --> " << itr->second << endl;
    }
}















