/*pointer.cpp*/
/*Program pokazujący wykorzystanie operatorów
adresu i wskaźnikowego.*/

#include<iostream>

using namespace std;

int main(){
    int x;          //deklaracja zmiennej całkowitej
    int* p_int;     //deklaracja zmiennej wskaźnikowej
    //obie zmienne nie zostały zainicjalizowane

    p_int = &x;     /*operatorem adresu odczytujemy, gdzie w pamięci
    znajduje się zmienna x, a następnie zapisujemy ten adres w zmiennej p_int*/

    cout << "Podaj liczbe: ";   //wczytanie wartości do zmiennej x
    cin >> x;

    cout << *p_int << endl;     /*wyświetlenie zmiennej x z wykorzystaniem
    operatora wskaźnikowego, który pozwala nam odczytać wartość spod adresu;
    w tym przypadktu to zmienna x, bo p_int pokazuje na x (linia 14)*/

    *p_int = 10;    /*przypisanie z wykorzystaniem operatora wskaźnikowego,
    modyfikowana jest zmienna x, bo p_int pokazuje na x.*/

    cout << x;      //wyświetlana jest liczba 10
}








