#ifndef MYTIME05_H_INCLUDED
#define MYTIME05_H_INCLUDED

#include<iostream>
using std::ostream;

class Time{
    public:
        Time();
        Time(int h, int m = 0);
        void AddMin(int m);
        void AddHr(int h);
        void Reset(int h = 0, int m = 0);
        Time operator+(const Time& t) const;
        Time operator-(const Time& t) const;
        Time operator*(double n) const;
        friend Time operator*(double n, const Time& t);
        //zaprzyjźniona funkcja przeciążająca operator <<
        //działający dla obiektów klasy ostream
        friend ostream& operator<<(ostream&, const Time& t);
    private:
        int _hours;
        int _minutes;
};


#endif // MYTIME05_H_INCLUDED
