#include <iostream>
#include "mytime05.h"

int main()
{
    using std::cout;
    using std::endl;
    Time weeding(4, 35);
    Time waxing(2, 47);
    Time total;
    Time diff;
    Time adjusted;

    //możliwe jest łatwe wyświetlanie czasu
    //z wykorzystaniem obiektu cout i przeciążonego
    //operatora <<
    cout << "czas pielenia = " << weeding << endl;
    cout << "czas woskowania = " << waxing << endl;

    total = weeding + waxing;
    cout << "razem czas pracy = " << total << endl;

    diff  = weeding - waxing;
    cout << "czas pielenia - czas woskowania = " << diff << endl;

    adjusted = 1.5 * total;
    cout << "czas pracy z poprawka na przerwy = " << adjusted << endl;

    return 0;
}












