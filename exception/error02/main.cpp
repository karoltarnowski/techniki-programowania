#include <iostream>
#include <cfloat>   //DBL_MAX

using namespace std;

//funkcja zwraca wartość true, jeśli obliczenia
//przebigły poprawnie albo false w przeciwnym przypadku,
//natomiast wynik zostaje zapisany w trzecim argumencie wywołania
//przekazanym przez wskaźnik
bool hmean(double a, double b, double * ans);

int main()
{
    double x, y, z;

    cout << "Podaj dwie liczby: ";
    while(cin >> x >> y){
        //wywołanie funkcji zwracającej status wykonania
        if( hmean(x, y, &z) )
            cout << "Srednia harmoniczna liczb " << x << " i " << y
                 << " wynosi " << z << endl;
        else
            cout << "Suma liczb nie moze wynosic 0 - "
                 << "Sprobuj jeszcze raz." << endl;
            cout << "Podaj kolejna pare liczb <w, aby wyjsc>: ";
    }
    cout << "Koniec" << endl;
    return 0;
}

bool hmean(double a, double b, double * ans){
    if( a == -b ){
        *ans = DBL_MAX;
        return false;
    }
    else{
        *ans = 2. * a * b / ( a + b );
        return true;
    }
}
