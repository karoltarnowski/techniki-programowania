#include <iostream>

using namespace std;

double hmean(double a, double b);

int main()
{
    double x, y, z;

    cout << "Podaj dwie liczby: ";
    while(cin >> x >> y){
        try{
            //wywołanie funkcji hmean() wewnątrz bloku try
            z = hmean(x, y);
        }
        //w bloku catch następuje "złapanie" wyjątku
        //i jego obsłużenie
        catch( const char* s ){
            cout << s << endl;
            cout << "Podaj kolejna pare liczb: ";
            continue;
        }
        cout << "Srednia harmoniczna liczb " << x << " i " << y
             << " wynosi " << z << endl;
        cout << "Podaj kolejna pare liczb <w, aby wyjsc>: ";
    }
    cout << "Koniec" << endl;
    return 0;
}

double hmean(double a, double b){
    if( a == -b )
        //zgłoszenie wyjątku w postaci ciągu znaków
        // z wykorzystaniem instrukcji throw
        throw "Niepoprawne argumenty funkcji hmean(): a = -b nie jest dozwolone";
    return 2. * a * b / ( a + b );
}
