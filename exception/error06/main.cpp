#include <iostream>
#include <cmath>
#include <exception>
#include "exc_mean.h"

using namespace std;

double hmean(double a, double b);
double gmean(double a, double b);

int main()
{
    double x, y, z;

    cout << "Podaj dwie liczby: ";
    while(cin >> x >> y){
        try{
            z = hmean(x, y);
            cout << "Srednia harmoniczna liczb " << x << " i " << y
                 << " wynosi " << z << endl;
            cout << "Srednia geometryczna liczb " << x << " i " << y
                 << " wynosi " << gmean(x,y) << endl;
            cout << "Podaj kolejna pare liczb <w, aby wyjsc>: ";
        }
        catch( exception& e ){
            cout << e.what() << endl;
            cout << "Sprobuj ponownie.";
        }
    }
    cout << "Koniec" << endl;
    return 0;
}

double hmean(double a, double b){
    if( a == -b )
        throw bad_hmean();
    return 2. * a * b / ( a + b );
}

double gmean(double a, double b){
    if( a < 0 || b < 0 )
        throw bad_gmean();
    return sqrt( a * b );
}
