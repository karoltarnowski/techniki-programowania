#include <iostream>
#include <cstdlib>  //abort()

using namespace std;

//deklaracja funkcji obliczającej średnią harmoniczną
double hmean(double a, double b);

//przykładowy program wykorzystujący funkcję hmean()
int main()
{
    double x, y, z;

    cout << "Podaj dwie liczby: ";
    while(cin >> x >> y){
        z = hmean(x, y);
        cout << "Srednia harmoniczna liczb " << x << " i " << y
             << " wynosi " << z << endl;
        cout << "Podaj kolejna pare liczb <w, aby wyjsc>: ";
    }
    cout << "Koniec" << endl;
    return 0;
}

//definicja funkcji hmean() wykorzystująca funckję abort()
//do obsługi sytuacji, w której a = -b.
double hmean(double a, double b){
    if( a == -b ){
        cout << "Niepoprawne argumenty funkcji hmean()" << endl;
        abort();
    }
    return 2. * a * b / ( a + b );
}
