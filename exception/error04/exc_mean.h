#ifndef EXC_MEAN_H_INCLUDED
#define EXC_MEAN_H_INCLUDED

#include <iostream>

//definicja klasy bad_hmean - wykorzystywanej jako wyjątek
//zgłaszany przy obliczaniu średniej harmonicznej
class bad_hmean{
    public:
        bad_hmean(double a = 0, double b = 0) : v1(a), v2(b){}
        void mesg();
    private:
        double v1;
        double v2;
};

inline void bad_hmean::mesg(){
    std::cout << "hmean(" << v1 << ", " << v2 << "): "
              << "niepoprawne argumenty: a = -b" << std::endl;
}

//definicja klasy bad_gmean - wykorzystywanej jako wyjątek
//zgłaszany przy obliczaniu średniej geometrycznej
class bad_gmean{
    public:
        bad_gmean(double a = 0, double b = 0) : v1(a), v2(b){}
        const char* mesg();
        double v1;
        double v2;
};

inline const char* bad_gmean::mesg(){
    return "Argumenty funkcji gmean() powinny byc >= 0\n";
}

#endif // EXC_MEAN_H_INCLUDED















