#include <iostream>
#include <cmath>
#include "exc_mean.h"

using namespace std;

double hmean(double a, double b);
double gmean(double a, double b);

int main()
{
    double x, y, z;

    cout << "Podaj dwie liczby: ";
    while(cin >> x >> y){
        //blok try zawiera wywołania funkcji hmean() oraz gmean()
        try{
            z = hmean(x, y);
            cout << "Srednia harmoniczna liczb " << x << " i " << y
                 << " wynosi " << z << endl;
            cout << "Srednia geometryczna liczb " << x << " i " << y
                 << " wynosi " << gmean(x,y) << endl;
            cout << "Podaj kolejna pare liczb <w, aby wyjsc>: ";
        }
        //blok catch - przechwytujący wyjątek typu bad_hmean
        catch( bad_hmean & bh ){
            bh.mesg();
            cout << "Sprobuj ponownie." << endl;
            continue;
        }
        //blok catch - przechwytujący wyjątek typu bad_gmean
        catch( bad_gmean & bg ){
            cout << bg.mesg();
            cout << "Uzyte wartosc: " << bg.v1 << ", "
                 << bg.v2 << endl;
            cout << "Koniec zabawy" << endl;
            break;
        }
    }
    cout << "Koniec" << endl;
    return 0;
}

double hmean(double a, double b){
    if( a == -b )
        throw bad_hmean(a, b);
    return 2. * a * b / ( a + b );
}

double gmean(double a, double b){
    if( a < 0 || b < 0 )
        throw bad_gmean(a, b);
    return sqrt( a * b );
}





















