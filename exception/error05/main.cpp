#include <iostream>
#include <cmath>
#include "exc_mean.h"
#include "demo.h"

using namespace std;

double hmean(double a, double b);
double gmean(double a, double b);
double means(double a, double b);

int main()
{
    double x, y, z;
    {
        demo d1("z bloku zagniezdzonego w funkcji main()");

        cout << "Podaj dwie liczby: ";
        while(cin >> x >> y){
            try{
                z = means(x, y);
                cout << "Srednia srednich liczb " << x << " i " << y
                     << " wynosi " << z << endl;
                cout << "Podaj kolejna pare liczb: ";
            }
            catch( bad_hmean & bh ){
                bh.mesg();
                cout << "Sprobuj ponownie." << endl;
                continue;
            }
            catch( bad_gmean & bg ){
                cout << bg.mesg();
                cout << "Uzyte wartosc: " << bg.v1 << ", "
                     << bg.v2 << endl;
                cout << "Koniec zabawy" << endl;
                break;
            }
        }
        d1.show();
    }
    cout << "Koniec" << endl;
    return 0;
}

double hmean(double a, double b){
    if( a == -b )
        throw bad_hmean(a, b);
    return 2. * a * b / ( a + b );
}

double gmean(double a, double b){
    if( a < 0 || b < 0 )
        throw bad_gmean(a, b);
    return sqrt( a * b );
}

double means(double a, double b){
    double am, hm, gm;
    demo d2("z funkcji means()");
    am = ( a + b ) / 2.0;
    try{
        hm = hmean(a, b);
        gm = gmean(a, b);
    }
    catch(bad_hmean & bh){
        bh.mesg();
        cout << "Przechwycony w means()" << endl;
        throw;
    }
    d2.show();
    return (am + hm + gm) / 3.0;
}




