#ifndef DEMO_H_INCLUDED
#define DEMO_H_INCLUDED
#include <string>

class demo{
    private:
        std::string word;
    public:
        demo (const char * s){
            word = s;
            std::cout << "Obiekt demo " << word << " utworzony." << std::endl;
        }
        ~demo(){
            std::cout << "Obiekt demo " << word << " usuniety." << std::endl;
        }
        void show() const{
            std::cout << "Obiekt demo " << word << " istnieje." << std::endl;
        }
};

#endif // DEMO_H_INCLUDED
