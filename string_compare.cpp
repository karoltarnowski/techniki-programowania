/*string_compare.cpp*/
/*Program pokazujący możliwość porównywania łańcuchów.*/

#include<iostream>
#include<string>

using namespace std;

int main(){
    string password;

    cout << "Podaj haslo: ";
    getline(cin, password, '\n');

    if( password == "olsah" ){
        cout << "Dostep przyznany" << endl;
    }
    else{
        cout << "Nieprawidlowe haslo. Odmowa dostępu!" << endl;
    }
}
