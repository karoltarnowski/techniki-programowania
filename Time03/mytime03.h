#ifndef MYTIME03_H_INCLUDED
#define MYTIME03_H_INCLUDED

class Time{
    public:
        Time();
        Time(int h, int m = 0);
        void AddMin(int m);
        void AddHr(int h);
        void Reset(int h = 0, int m = 0);
        Time operator+(const Time& t) const;
        //przeciążony operator odejmowania
        Time operator-(const Time& t) const;
        //przeciążony operator mnożenia przez liczbę rzeczywistą
        Time operator*(double n) const;
        void Show() const;
    private:
        int _hours;
        int _minutes;
};

#endif // MYTIME03_H_INCLUDED
