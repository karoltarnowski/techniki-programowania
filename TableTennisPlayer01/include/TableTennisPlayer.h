#ifndef TABLETENNISPLAYER_H
#define TABLETENNISPLAYER_H
#include <string>

class TableTennisPlayer
{
    public:
        TableTennisPlayer(const std::string & fn = "brak", \
                          const std::string & ln = "brak", \
                          bool ht = false);
        void name() const;
        bool hasTable() const { return _has_table; };
        void resetTable(bool v) { _has_table = v; };

    //klasa zawiera informacje o:
    //  imieniu gracza,
    //  nazwisku gracza,
    //  czy posiada stół do gry
    private:
        std::string _first_name;
        std::string _last_name;
        bool   _has_table;
};

#endif // TABLETENNISPLAYER_H
