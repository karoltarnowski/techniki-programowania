#include "TableTennisPlayer.h"
#include <iostream>
using std::string;
using std::cout;

TableTennisPlayer::TableTennisPlayer(const string & fn, const string & ln, bool ht)
    : _first_name(fn) \
    , _last_name(ln)  \
    , _has_table(ht) {}

void TableTennisPlayer::name() const{
    cout << _last_name << ", " << _first_name;
}
