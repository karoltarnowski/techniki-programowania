#include <iostream>
#include "include\TableTennisPlayer.h"

int main()
{
    TableTennisPlayer player1("Jacek","Pogodny", true);
    TableTennisPlayer player2("Teresa","Bogatko", false);

    player1.name();
    if(player1.hasTable())
        std::cout << ": posiada stol." << std::endl;
    else
        std::cout << ": nie posiada stolu." << std::endl;

    player2.name();
    if(player2.hasTable())
        std::cout << ": posiada stol." << std::endl;
    else
        std::cout << ": nie posiada stolu." << std::endl;

    return 0;
}
