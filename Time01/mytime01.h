#ifndef MYTIME01_H_INCLUDED
#define MYTIME01_H_INCLUDED

//definicja klasy Time
class Time{
    //deklaracja interfejsu publicznego
    public:
        Time();                            //konstruktor
        Time(int h, int m = 0);            //konstruktor o dwóch argumentach (godziny, minuty)
        void AddMin(int m);                //dodawanie minut do czasu
        void AddHr(int h);                 //dodawanie godzin do czasu
        void Reset(int h = 0, int m = 0);  //ustawianie (resetowanie) czasu
        Time Sum(const Time& t) const;     //dodawanie dwóch czasów
        void Show() const;                 //wyświetlenie czasu
    //deklaracja pól prywatnych przechowujących czas w formacie godziny, minuty
    private:
        int _hours;
        int _minutes;
};

#endif // MYTIME01_H_INCLUDED
