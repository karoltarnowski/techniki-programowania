#include <iostream>
#include "mytime01.h"

//konstruktor z listą inicjalizacyjną
Time::Time()
    : _hours(0)
    , _minutes(0)
{}

//konstruktor o dwóch argumentach (godziny, minuty)
Time::Time(int h, int m)
    : _hours(h)
    , _minutes(m)
{}

//operacja dodawania minut uwzględniająca, że minuty powinny się mieścić poniżej 60
void Time::AddMin(int m){
    _minutes += m;
    _hours += _minutes / 60;
    _minutes %= 60;
}

//operacja dodawania godzin
void Time::AddHr(int h){
    _hours += h;
}

//ustawienie czasu na zadane wartości (godziny, minuty)
void Time::Reset(int h, int m){
    _hours = h;
    _minutes = m;
}

//dodawanie dwóch czasów
//metoda jest wywoływana dla konkretnej instancji obiektu
//a drugi obiekt jest przekazywany jako argument wywołania
Time Time::Sum(const Time& t) const{
    Time sum;
    sum._minutes = _minutes + t._minutes;
    sum._hours   = _hours   + t._hours  + sum._minutes/60;
    sum._minutes %= 60;
    return sum;
}

//wyświetlenie czasu
void Time::Show() const{
    std::cout << _hours << " godzin, " << _minutes << " minut";
}













