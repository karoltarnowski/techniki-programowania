#include <iostream>
#include "mytime01.h"

/*Przykładowy kod wykorzystujący możliwość klasy Time*/

int main()
{
    using std::cout;
    using std::endl;
    Time planning;
    Time coding(2, 40);
    Time fixing(5, 55);
    Time total;

    cout << "czas planowania = ";
    planning.Show();
    cout << endl;

    cout << "czas kodowania = ";
    coding.Show();
    cout << endl;

    cout << "czas poprawiania = ";
    fixing.Show();
    cout << endl;

    //metoda Sum() jest wywoływana dla obiektu coding
    //z argumentem fixing
    //wynik jest zapisywany w obiekcie total
    //wykorzystany jest niejawny operator przypisania
    total = coding.Sum(fixing);

    cout << "razem (coding.sum(fixing)) = ";
    total.Show();
    cout << endl;

    return 0;
}

















