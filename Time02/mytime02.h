#ifndef MYTIME02_H_INCLUDED
#define MYTIME02_H_INCLUDED

class Time{
    public:
        Time();
        Time(int h, int m = 0);
        void AddMin(int m);
        void AddHr(int h);
        void Reset(int h = 0, int m = 0);
        //metoda Sum() została zastąpiona przeciążonym operatorem dodawania
        Time operator+(const Time& t) const;
        void Show() const;
    private:
        int _hours;
        int _minutes;
};

#endif // MYTIME02_H_INCLUDED
