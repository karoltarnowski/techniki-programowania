#include <iostream>
#include "mytime02.h"

Time::Time()
    : _hours(0)
    , _minutes(0)
{}

Time::Time(int h, int m)
    : _hours(h)
    , _minutes(m)
{}

void Time::AddMin(int m){
    _minutes += m;
    _hours += _minutes / 60;
    _minutes %= 60;
}

void Time::AddHr(int h){
    _hours += h;
}

void Time::Reset(int h, int m){
    _hours = h;
    _minutes = m;
}

Time Time::operator+(const Time& t) const{
    Time sum;
    sum._minutes = _minutes + t._minutes;
    sum._hours   = _hours   + t._hours  + sum._minutes/60;
    sum._minutes %= 60;
    return sum;
}

void Time::Show() const{
    std::cout << _hours << " godzin, " << _minutes << " minut";
}













