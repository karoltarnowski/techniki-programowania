#ifndef RATEDPLAYER_H
#define RATEDPLAYER_H

#include <TableTennisPlayer.h>

//definicja klasy RatedPlayer
//dziedziczącej po klasie TableTennisPlayer
class RatedPlayer : public TableTennisPlayer
{
    public:
        RatedPlayer(unsigned int r = 0, \
                    const string & fn = "brak", \
                    const string & ln = "brak", \
                    bool ht = false);
        RatedPlayer(unsigned int r, \
                    const TableTennisPlayer & tp);
        unsigned int rating() const { return _rating; };
        void resetRating(unsigned int r){ _rating = r; };

    //klasa RatedPlayer zawiera wszystkie pola klasy TableTennisPlayer
    //oraz dodatkowo pole _rating
    private:
        unsigned int _rating;
};

#endif // RATEDPLAYER_H
