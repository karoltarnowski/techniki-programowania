#include <iostream>
#include "include\TableTennisPlayer.h"
#include "include\RatedPlayer.h"

int main()
{
    TableTennisPlayer  player1("Teresa", "Bogatko", false);
    RatedPlayer       rplayer1(1140, "Maciej", "Kaczkowski", true);

    //dla obiektu klasy RatedPlayer można wywoływać
    //metody klasy TableTennisPlayer
    rplayer1.name();
    if(rplayer1.hasTable())
        std::cout << ": posiada stol." << std::endl;
    else
        std::cout << ": nie posiada stolu." << std::endl;

    player1.name();
    if(player1.hasTable())
        std::cout << ": posiada stol." << std::endl;
    else
        std::cout << ": nie posiada stolu." << std::endl;

    std::cout << "Nazwisko i imie: ";
    rplayer1.name();
    //dodatkowo w klasie RatedPlayer jest metoda rating()
    std::cout << "; Ranking: " << rplayer1.rating() << std::endl;

    //RatedPlayer ma też konstruktor pobierający
    //jako argument obiekt klasy TableTennisPlayer
    RatedPlayer rplayer2(1212, player1);
    std::cout << "Nazwisko i imie: ";
    rplayer2.name();
    std::cout << "; Ranking: " << rplayer2.rating() << std::endl;

    return 0;
}
