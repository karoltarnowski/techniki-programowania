#include "RatedPlayer.h"

RatedPlayer::RatedPlayer(unsigned int r, \
  const string & fn, const string & ln, bool ht)
  : TableTennisPlayer(fn, ln, ht)
{
    //_first_name = fn;
    _rating = r;
}

/*
pominięcie konstruktora klasy bazowej powodowałoby
wywołanie konstruktura domyślnego

RatedPlayer::RatedPlayer(unsigned int r, \
  const string & fn, const string & ln, bool ht)
{
    _rating = r;
}
*/

RatedPlayer::RatedPlayer(unsigned int r, const TableTennisPlayer & tp)
    : TableTennisPlayer(tp)
{
    _rating = r;
}

/*
powyższy konstruktor jest równoważny następującemu
RatedPlayer::RatedPlayer(unsigned int r, const TableTennisPlayer & tp)
    : TableTennisPlayer(tp)
    , _rating(r)
{
}*/












