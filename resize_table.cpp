/*resize_table.cpp*/
/*Przykładowy program wykorzystujący
dynamiczną alokację pamięci.
W dynamicznie alokowanej tablicy numbers
zbierane są liczby podane przez uźytkownika.
Tablica ma długość length,
a zmienna next przechowuje liczbę
wpisanych danych (jednocześnie
indeks następnego elementu).*/

#include <iostream>

using namespace std;

/*Funkcja drukuje informacje o tablicy numbers:
jej długość (length), liczbę elementów (filled),
oraz zawartość.*/
void printTable(int* numbers, int length, int filled);

/*Funkcja przepisuje tablicę numbers do
nowoalokowanej tablicy o dwa razy większej długości.
Zwalnia nie używaną pamięć i zwraca adres
nowej tablicy, oraz aktualizuje długość (length)
przekazaną przez referencję.*/
int* enlargeTable(int* numbers, int& length);

int main(){
    int next = 0;   //początkowo nie ma elementów w tablicy
    int length = 8; //początkowa długość = 8
    int *numbers = new int[length]; //odpowiednia alokacja
    int num;
    cout << "Podaj liczbe (0 - wyjscie z programu): ";
    cin  >> num;
    while(num!=0){
        //jeśli kolejny element nie zmieści się w tablicy
        if( length == next ){
            //to powiększ tablicę
            numbers = enlargeTable(numbers, length);
        }
        //wpisz nowy element do tablicy i przesuń indeks next
        numbers[next++] = num;
        cout << "Aktualny stan tablicy" << endl;
        //wyświetlenie informacji o tablicy
        printTable(numbers,length,next);
        cout << "Podaj liczbe (0 - wyjscie z programu): ";
        cin  >> num;
    }
    //zwolnienie pamięci
    delete[] numbers;
}

void printTable(int* numbers, int length, int filled){
    cout << "Rozmiar tablicy: " << length << endl;
    cout << "Zajete pola: "     << filled << endl;
    cout << "Liczby w tablicy:" << endl;
    for(int i=0; i<filled; i++){
        cout << "numbers[" << i << "] = " << numbers[i] << endl;
    }
}

int* enlargeTable(int* numbers, int& length){
    //alokacja nowej - dwa razy większej tablicy
    int* newNumbers = new int[length*2];
    //przepisanie elementów do nowej tablicy
    for(int i=0; i<length; i++){
        newNumbers[i] = numbers[i];
    }
    //aktualizacja dłguości
    length *= 2;
    //zwolnienie starej pamięci
    delete[] numbers;
    //zwracana jest nowa tablica
    return newNumbers;
}
















