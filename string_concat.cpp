/*string_concat.cpp*/
/*Program pokazujący możliwość łączenia łańcuchów.*/

#include<iostream>
#include<string>

using namespace std;

int main(){
    string name;
    string surname;

    cout << "Podaj swoje imie: ";
    cin >> name;
    cout << "Podaj swoje nazwisko: ";
    cin >> surname;

    string full_name = name + " " + surname;
    /*Operator + pozwala konkatenować (łączyć) łańcuchy
    trzy łańcuchy: name, " " (spacja) oraz surname,
    łączone są w jeden łańcuch.*/

    cout << "Nazywasz sie: " << full_name << endl;
}
