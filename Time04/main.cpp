#include <iostream>
#include "mytime04.h"

int main()
{
    using std::cout;
    using std::endl;
    Time weeding(4,35);
    Time waxing(2, 47);
    Time total;
    Time diff;
    Time adjusted;

    cout << "czas pielenia = ";
    weeding.Show();
    cout << endl;

    cout << "czas woskowania = ";
    waxing.Show();
    cout << endl;

    total = weeding + waxing;
    cout << "razem czas pracy = ";
    total.Show();
    cout << endl;

    diff = weeding - waxing;
    cout << "czas pielenia - czas woskowania = ";
    diff.Show();
    cout << endl;

    adjusted = 1.5 * total;
    cout << "czas pracy z poprawka na przerwy = ";
    adjusted.Show();
    cout << endl;

    return 0;
}












