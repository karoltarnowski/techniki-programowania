#ifndef MYTIME04_H_INCLUDED
#define MYTIME04_H_INCLUDED

class Time{
    public:
        Time();
        Time(int h, int m = 0);
        void AddMin(int m);
        void AddHr(int h);
        void Reset(int h = 0, int m = 0);
        Time operator+(const Time& t) const;
        Time operator-(const Time& t) const;
        //przeciążony operator mnożenia obsługuje
        //mnożenie czas * liczba
        Time operator*(double n) const;
        //dodano zaprzyjaźnioną funkcję przeciążąjącą
        //operator mnożenia w celu obsługi mnożenia
        //liczba * czas
        friend Time operator*(double n, const Time& t);
        void Show() const;
    private:
        int _hours;
        int _minutes;
};


#endif // MYTIME04_H_INCLUDED
