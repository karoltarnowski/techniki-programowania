#include <iostream>
#include "mytime04.h"

Time::Time()
    : _hours(0)
    , _minutes(0)
{}

Time::Time(int h, int m)
    : _hours(h)
    , _minutes(m)
{}

void Time::AddMin(int m){
    _minutes += m;
    _hours += _minutes / 60;
    _minutes %= 60;
}

void Time::AddHr(int h){
    _hours += h;
}

void Time::Reset(int h, int m){
    _hours = h;
    _minutes = m;
}

Time Time::operator+(const Time& t) const{
    Time sum;
    sum._minutes = _minutes + t._minutes;
    sum._hours   = _hours   + t._hours  + sum._minutes/60;
    sum._minutes %= 60;
    return sum;
}

Time Time::operator-(const Time& t) const{
    Time diff;
    int tot1, tot2;
    tot1 = t._minutes + 60*t._hours;
    tot2 =   _minutes + 60*  _hours;
    diff._minutes = (tot2 - tot1)%60;
    diff._hours   = (tot2 - tot1)/60;
    return diff;
}

Time Time::operator*(double mult) const{
    Time result;
    long totalMinutes = _hours*mult*60 + _minutes*mult;
    result._minutes = totalMinutes%60;
    result._hours   = totalMinutes/60;
    return result;
}

Time operator*(double m, const Time& t){
    Time result;
    long totalMinutes = t._hours*m*60 + t._minutes*m;
    result._minutes = totalMinutes%60;
    result._hours   = totalMinutes/60;
    return result;
}


void Time::Show() const{
    std::cout << _hours << " godzin, " << _minutes << " minut";
}













