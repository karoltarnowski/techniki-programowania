/*bool_example.cpp*/
/*Program pokazujący typ bool.*/

#include<iostream>

using namespace std;

int main(){
    int x;

    cout << "Podaj liczbe: ";
    cin >> x;

    bool x_equals_two = x == 2;
    /*Wynik porównania x == 2 jest przypisywany
    do zmiennej x_equals_two typu logicznego (bool).*/

    /*Dalsze instrukcje wykonywane w zależności
    od wartości zmiennej x_equals_two.*/
    if( x_equals_two )
        cout << "x = 2" << endl;
}
