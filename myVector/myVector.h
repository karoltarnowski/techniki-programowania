/*Plik nagłówkowy definiujący szablon klasy myVector.*/
#ifndef MYVECTOR_H_INCLUDED
#define MYVECTOR_H_INCLUDED

//szablon klasy, który zależy od typu T
template <typename T>
class myVector{
    public:
        myVector();
        myVector(int size);
        ~myVector();
        //deklaracja przeciążonego operatora [],
        //który zwraca referencję do typu T
        T& operator[](int index);
        const T& operator[](int index) const;
        int size() const;

    private:
        //klasa przechowuje n elementów typu T
        //w tablicy wskazywanej przez data
        //data jest wskaźnikiem na typ T
        T* data;
        int n;
};

//szablon konstruktora wektora pustego
template <typename T>
myVector<T>::myVector():data(NULL),n(0){}

//szablon konstruktora wektora n-elementowego
template <typename T>
myVector<T>::myVector(int size):n(size){
    data = new T[n];
}

//szablon destruktor
template <typename T>
myVector<T>::~myVector(){
    delete[] data;
}

//szablon przeciążonego operatora []
template <typename T>
T& myVector<T>::operator[](int index){
    return data[index];
}

//szablon przeciążonego operatora [] dla stałego wektora
template <typename T>
const T& myVector<T>::operator[](int index) const{
    return data[index];
}

//szablon metody size() - ciało funkcji nie zależy od typu T,
//ale zakres metody myVector<T> zależy.
template <typename T>
int myVector<T>::size() const{
    return n;
}

//tak zdefiniowany szablon można rozszerzyć np. o metody pozwalające
//na zmienianie rozmiarów wektora

#endif // MYVECTOR_H_INCLUDED
