/*Program ilustruje wykorzystanie szablonu klasy myVector
zdefiniownego w pliku myVector.h*/
#include <iostream>
#include "myVector.h"

using namespace std;

int main()
{
    //deklaracja wektora liczb całkowitych
    myVector<int> vec(8);
    //w szablonie klasy zdefiniowano metodę size()
    for(int i=0; i<vec.size(); i++)
        //przeciążono także operator[],
        //co pozwala na dostęp do składowych wektora
        vec[i] = i*i;
    for(int i=0; i<vec.size(); i++)
        cout << vec[i] << " ";
    cout << endl;

    //deklaracja wektora zmiennych znakowych
    myVector<char> alphabet(26);
    for(int i=0; i<alphabet.size(); i++)
        alphabet[i] = 'a' + i;
    for(int i=0; i<alphabet.size(); i++)
        cout << alphabet[i] << " ";

}
