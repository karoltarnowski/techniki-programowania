/*short_circuit_evaluation.cpp*/
/*Program ilustruje wykorzystanie
skróconego wartościowania przy obliczaniu
wartości logicznej warunku*/

#include<iostream>

using namespace std;

int main(){
    float x;

    cout << "Podaj x: ";
    cin >> x;

    /*Prawdziwość drugiego zdania koniunkcji (5/x < 1)
    jest sprawdzana tylko wtedy, gdy pierwsze zdanie (x!=0)
    jest prawdziwe.
    Jeżeli pierwsze zdanie jest fałszywe to całe wyrażenie
    jest fałszywe niezależnie od oceny drugiego zdania.

    W tym przypadku jeśli x == 0 nie jest wykonywane działanie 5/x.

    Uruchom program podając różne wartości x, np.: 0, 2, -2, 8.*/
    if( x!=0 && 5/x < 1 ){
        cout << "5/x = " << 5/x;
    }

}
