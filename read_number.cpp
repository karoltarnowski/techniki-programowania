/*read_number.cpp*/

#include<iostream>

using namespace std;

int main(){
    int number;                 //deklaracja zmiennej typu całkowitego

    cout << "Podaj liczbe: ";

    cin >> number;
    //wczytanie danych ze strumienia wejściowego do zmiennej number

    cout << "Wprowadzona liczba to: " << number << endl;
    /*wyświetlenie wczytanych danych na ekranie
    Zwróć uwagę na wielokrotne powtórzenie operatora wstawiania,
    co powoduje wypisanie sekwencyjne danych.*/
}
