/*for_example.cpp*/
/*Program pokazujący możliwość deklarowania zmiennej w pętli for.*/

#include<iostream>

using namespace std;

int main(){
    for(int i=0; i<10; i++){
        //Zwróć uwagę, że zmienna i jest deklarowana w instrukcji inicjalizacji.
        cout << i << endl;
    }
    /*Odwołanie się do zmiennej i poza pętlą nie jest możliwe,
    ze względu na ogranicznie zasięgu zmiennej.*/
}
