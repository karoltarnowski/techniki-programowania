#include <fstream>
#include <iostream>

using namespace std;

int main(){
    ifstream read_file("myfile.txt");
    if( !read_file.is_open() ){
        cout << "Blad otwarcia pliku!" << endl;
        return -1;
    }

    int number;
    //sprawdzenie poprawnego odczytu danych
    if( read_file >> number )
        cout << "Wczytana wartosc to: " << number;

    return 0;
}
