#include <fstream>
#include <iostream>
#include <vector>

using namespace std;

int main(){
    ifstream read_file("results.txt");
    if( !read_file.is_open() ){
        cout << "Blad otwarcia pliku!" << endl;
        return -1;
    }

    vector<int> results;
    for(int i=0; i<10; i++){
        int result;
        read_file >> result;
        results.push_back(result);
    }

    return 0;
}
