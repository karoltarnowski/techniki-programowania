#include<fstream>
#include<iostream>
#include<vector>

using namespace std;

int main(){
    //otwarcie pliku do zapisu/odczytu
    fstream file("results2.txt", ios::in | ios::out );
    if( !file.is_open() ){
        cout << "Blad otwarcia pliku!" << endl;
        return -1;
    }

    int new_result;
    cout << "Podaj nowy wynik: ";
    cin  >> new_result;

    //zapamiętujemy pozycję w pliku przed aktualnym wynikiem
    streampos prev_result_pos = file.tellg();
    int current_result;
    //wczytujemy kolejne wyniki
    while( file >> current_result ){
        //dopóki nie trafimy na mniejszy, niż nowy wynik
        if( current_result < new_result ){
            break;
        }
        prev_result_pos = file.tellg();
    }

    //sprawdzamy, czy przerwanie pętli nastąpiło z powodu
    //błędu odczytu lub końca pliku
    if( file.fail() && !file.eof() ){
        cout << "Blad odczytu pliku!";
        return -2;
    }

    //czyścimy status błedów
    file.clear();

    //ustawiamy pozycję odczytu w pliku
    //na zapamiętaną pozycję
    //przed pierwszym wynikiem mniejszym niż nowy
    file.seekg( prev_result_pos );

    //wczytujemy kolejne wyniki do wektora
    vector<int> results;
    while( file >> current_result ){
        results.push_back(current_result);
    }

    //upewniamy się, że dotarliśmy do końca pliku
    if( !file.eof() ){
        cout << "Blad odczytu pliku!";
        return 0;
    }

    //czyścimy status błedów
    file.clear();

    //ponownie ustawiamy pozycję odczytu w pliku
    //na zapamiętaną pozycję
    file.seekp(prev_result_pos);

    //jeśli nie jesteśmy na początku pliku to dostawiamy spację
    if( prev_result_pos != std::streampos(0) ){
        file << " ";
    }
    //zapisujemy nowy wynik
    file << new_result << " ";

    //a następnie wypisujemy zawartość wektora
    for(vector<int>::iterator itr = results.begin(), end = results.end();
        itr != end;
        ++itr){
        file << *itr << " ";
    }
}






