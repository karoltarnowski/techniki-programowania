/*Przykładowy program ilustrujący przekazywanie
argumentów linii poleceń. Program wypisuje
na standardowe wyjście zawartość pliku,
którego nazwę przekazano jako argument linii poleceń.*/
#include <fstream>
#include <iostream>

using namespace std;

//argc - liczba argumentów w linii poleceń
//argv - tablica wskaźników znakowych
int main(int argc, char *argv[])
{
    //program oczekuje wywołania o jednym argumencie
    //argv[0] - to nazwa programu
    //argv[1] - to nazwa pliku
    if( argc != 2 ){
        cout << "Wywolanie: " << argv[0] << " <nazwa pliku>" << endl;
    }
    else{
        //otwarcie pliku do odczytu
        ifstream file( argv[1] );
        //sprawdzenie czy plik został otwarty
        if( !file.is_open() ){
            cout << "Blad otwarcia pliku " << argv[1] << endl;
            return -1;
        }
        char c;
        //odczytywanie pliku znak po znaku
        while( file.get(c) ){
            cout << c;
        }
    }
    return 0;
}


