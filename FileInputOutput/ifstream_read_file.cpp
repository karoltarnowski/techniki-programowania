#include <fstream>
#include <iostream>

using namespace std;

int main(){
    ifstream read_file("myfile.txt");
    //jeśli nie udało się otworzyć pliku kończymy
    //działanie programu
    if( !read_file.is_open() ){
        cout << "Blad otwarcia pliku!" << endl;
        return -1;
    }

    //w przeciwnym przypadku wczytujemy z pliku liczbę
    int number;
    read_file >> number;

    return 0;
}
