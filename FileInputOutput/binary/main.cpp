#include <fstream>
#include <string>
#include <iostream>

using namespace std;

struct Player{
    int age;
    int score;
    string name;
};

int main()
{
    ///deklaracja i inicjalizacja struktury danych
    Player player, player2;
    player.age = 19;
    player.score = 200;
    player.name = "John";

    ///zapis do pliku
    //otworzenie pliku do odczytu/zapisu w trybie binarnym z nadpisaniem pliku
    fstream file( "data.bin", ios::trunc | ios::binary | ios::in | ios:: out );

    //zapis pola age
    file.write( reinterpret_cast<char*>(&player.age),   sizeof(player.age) );

    //zapis pola score
    file.write( reinterpret_cast<char*>(&player.score), sizeof(player.score) );

    //zapis pola name
    //zapis długości łańcucha ...
    int length = player.name.length();
    file.write( reinterpret_cast<char*>(&length),   sizeof(length) );
    //... oraz jego zawartości
    file.write( player.name.c_str(), player.name.length() + 1 );

    ///odczytanie danych z pliku
    //ustawienie pozycji z pliku na początek
    file.seekg( 0, ios::beg );
    //odczyt sizeof(player2.age) bajtów z pliku bajt po bajcie pod adres &player2.age
    if( !file.read( reinterpret_cast<char*>(&player2.age),   sizeof(player2.age) ) ){
        cout << "Blad odczytu z pliku" << endl;
        return -2;
    }
    //odczyt wyniku
    if( !file.read( reinterpret_cast<char*>(&player2.score), sizeof(player2.score) ) ){
        cout << "Blad odczytut z pliku" << endl;
        return -2;
    }

    //odczyt nazwy
    int length2;
    //długość łańucha
    if( !file.read( reinterpret_cast<char*>(&length2) , sizeof(length2) ) ){
        cout << "Blad odczytu z pliku" << endl;
        return -2;
    }

    //i sam łańcuch
    if( length2 > 0 && length2 < 10000 ){
        //wykorzystując pomocniczą tablicę
        char* string_buf = new char[ length2+1 ];
        if( !file.read( string_buf, length2 + 1 ) ){
            delete[] string_buf;
            cout << "Blad odczytu z pliku" << endl;
            return -2;
        }
        if( string_buf[length2] == 0 ){
            player2.name = string(string_buf);
        }
        delete[] string_buf;
    }

    cout << player2.age << " " << player2.score << " " << player2.name << endl;

    return 0;
}

