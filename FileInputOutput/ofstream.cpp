#include <fstream>
#include <iostream>
#include <vector>

using namespace std;

int main(){
    //otwarcie pliku do zapisu
    ofstream write_file("results2.txt");

    if( !write_file.is_open() ){
        cout << "Blad otwarcia pliku!" << endl;
        return -1;
    }

    //wypisanie do pliku liczb od 10 do 1
    for(int i=10; i>0; i--){
        write_file << i << " ";
    }

    return 0;
}
