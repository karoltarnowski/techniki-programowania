#include <fstream>
#include <iostream>

using namespace std;

int main(){
    ifstream read_file("myfile.txt");
    //metoda is_open() pozwala sprawdzić,
    //czy udało się otworzyć plik
    if( !read_file.is_open() )
        cout << "Blad otwarcia pliku!" << endl;
    return 0;
}
